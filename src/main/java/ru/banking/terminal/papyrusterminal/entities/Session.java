package ru.banking.terminal.papyrusterminal.entities;

import java.util.Date;

/**
 *
 * @author roux
 */
public class Session {
    private String sessionId;
    private String login;
    private String userName;
    private Date lastRefreshDate; 
    
    /**
     * Конструктор
     * @param sessionId
     * @param login
     * @param userName 
     */
    public Session(String sessionId, String login, String userName) {
        this.sessionId = sessionId;
        this.login = login;
        this.userName = userName;
        this.lastRefreshDate = new Date();
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the lastRefreshDate
     */
    public Date getLastRefreshDate() {
        return lastRefreshDate;
    }

    /**
     * @param lastRefreshDate the lastRefreshDate to set
     */
    public void setLastRefreshDate(Date lastRefreshDate) {
        this.lastRefreshDate = lastRefreshDate;
    }
    
    public String toString() {
        return "sessionId: " + this.sessionId + ", login: " + this.login + ", userName: " + this.userName + ", lastRefreshDate: " + this.lastRefreshDate;
    } 
}
