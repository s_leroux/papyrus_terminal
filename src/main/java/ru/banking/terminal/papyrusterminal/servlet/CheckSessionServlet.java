package ru.banking.terminal.papyrusterminal.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ru.banking.terminal.papyrusterminal.App;

/**
 *  Проверяем сессию по заданному sessionId
 * @author roux
 */
public class CheckSessionServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(CheckSessionServlet.class);
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        LOGGER.trace("Это doPost RemoteAddr: " + request.getRemoteAddr() + ", sessionId: " + request.getParameter("sessionId"));
        
        App app = App.INSTANCE;
        
        /* Пока всегда говорим, что сессия наисвежайшая */
        String userName = "", login = "", sessionState= "invalid";
        
        if(app.getSessions().size() > 0 && app.getSessions().keySet().contains(request.getParameter("sessionId"))) {
            sessionState = "valid";
            login = app.getSessions().get(request.getParameter("sessionId")).getLogin();
            userName = app.getSessions().get(request.getParameter("sessionId")).getUserName();
        }
        
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        
        JSONObject root = new JSONObject();
        root.put("sessionState", sessionState);
        root.put("userName", userName);
        root.put("login", login);
        
//        LOGGER.trace("Сконструирован ответ: " + root.toString());
        
        try {
            out.print(root.toString());
        } finally {
            out.close();
        }
    }
}
