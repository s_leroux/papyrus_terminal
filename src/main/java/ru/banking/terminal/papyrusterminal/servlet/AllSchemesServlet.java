package ru.banking.terminal.papyrusterminal.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ru.banking.terminal.papyrusterminal.App;

/**
 *
 * @author roux
 */
public class AllSchemesServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(AllSchemesServlet.class);
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        LOGGER.trace("Это doGet, selectedScheme: " + request.getParameter("selectedScheme"));
        String selectedScheme = request.getParameter("selectedScheme");
        
        JSONObject root = new JSONObject();
        
        JSONArray schemesArray = new JSONArray();
        HashMap<String, String[]> schemes = App.getCOLOR_SCHEMES();
        for (String schemeName : schemes.keySet()) {
            JSONObject scheme = new JSONObject();
            scheme.put("name", schemeName);
            scheme.put("colors", schemes.get(schemeName));
            schemesArray.put(scheme);
        }
        root.put("selected_scheme", selectedScheme == null?"grayscale":selectedScheme);
        root.put("color_schemes", schemesArray);
        
//        response.setContentType("application/json");
        response.setContentType("text/html;charset=UTF-8");
    
        PrintWriter out = response.getWriter();
        try {
            out.println(root);
        } finally {
            out.close();
        }
    }
}
