package ru.banking.terminal.papyrusterminal.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ru.banking.terminal.papyrusterminal.App;

/**
 *
 * @author roux
 */
public class ColorSchemeServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(ColorSchemeServlet.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        LOGGER.trace("Это doGet, request scheme: " + request.getParameter("scheme"));
        
        String schemeName = request.getParameter("scheme");
        String[] scheme = App.getCOLOR_SCHEMES().get(schemeName);
        if(scheme == null) {
            scheme = App.getCOLOR_SCHEMES().get("grayscale");
        }
        
        StringBuilder outSB = new StringBuilder();
        outSB.append("body {\n\tborder-color: " + scheme[3] + ";\n}\n");
        outSB.append("input[type=text], input[type=password] {\n\tborder-color: " + scheme[3] + ";\n}\n");
        outSB.append(".upper-band {\n\tbackground-color: " + scheme[4] + ";\n\tborder-bottom-color: " + scheme[3] + ";\n}\n");
        outSB.append("#band-title {\n\tcolor: " + scheme[0] + ";\n}\n");
        outSB.append("#band-title:hover {\n\tcolor: white;\n}\n");
        outSB.append("#band-title:active {\n\tcolor: " + scheme[2] + ";\n}\n");
        outSB.append(".control-pane {\n\tbackground-color: " + scheme[2] + ";\n\tborder-bottom-color: " + scheme[3] + ";\n}\n");
        outSB.append("#properties-button {\n\tbackground-color: " + scheme[2] + ";\n}\n");
        outSB.append("#properties-button:hover {\n\tbackground-color: " + scheme[1] + ";\n}\n");
        outSB.append("#properties-button:active {\n\tbackground-color: " + scheme[4] + ";\n}\n");
        outSB.append(".jq-dropdown-menu li {\n\tcolor: " + scheme[3] + ";\n}\n");

        outSB.append(".ui-widget-header {\n\tcolor: " + scheme[4] + " !important;\n\tbackground-color: " + scheme[2] + " !important;\n}\n");
        outSB.append(".ui-dialog {\n\tbackground-color: white !important;\n}\n");
        
        outSB.append(".w2ui-popup {\n\tborder-color: " + scheme[0] + " !important;\n}\n");
        outSB.append(".w2ui-floatable {\n\tborder-color: " + scheme[0] + " !important;\n}\n");
        outSB.append(".w2ui-field > label {\n\tcolor: " + scheme[4] + " !important;\n}\n");
        
        outSB.append(".w2ui-form .w2ui-buttons {\n\tborder-top-color: " + scheme[2] + " !important;\n}\n");
        outSB.append("button.w2ui-btn {\n\tcolor: " + scheme[4] + " !important;\n\tborder-color: " + scheme[2] +" !important;\n}\n");
        outSB.append("button.w2ui-btn:focus:before {\n\tborder-color: " + scheme[2] + " !important;\n}\n");
        
        outSB.append(".w2ui-popup .w2ui-popup-title {\n\tcolor: " + scheme[4] + " !important;\n\tbackground-color: " + scheme[2] + " !important;\n}\n");
        outSB.append(".w2ui-popup .w2ui-popup-body {\n\tbackground-color: " + scheme[0]+  " !important;\n}\n");
        outSB.append(".w2ui-floatable .w2ui-popup-title {\n\tcolor: " + scheme[4] + " !important;\n\tbackground-color: " + scheme[2] + " !important;\n}\n");
        outSB.append(".w2ui-floatable .w2ui-popup-body {\n\tbackground-color: " + scheme[0]+  " !important;\n}\n");
        outSB.append("input:not([type=button]):not([type=submit]).w2ui-input, textarea.w2ui-input {\n\tborder-color: " + scheme[3] + ";\n}\n");
        
        outSB.append(".splitter_bar {\n\tbackground-color: " + scheme[2] + " !important;\n}\n");
        outSB.append(".color_specimen {\n\tborder-color: " + "white" + ";\n}\n");
        outSB.append(".treeview span:hover {\n\tcolor: " + scheme[2] + ";\n}\n");
        outSB.append(".treeview span:active {\n\tcolor: " + scheme[3] + ";\n}\n");
        outSB.append(".about-dialog {\n\tborder-color: " + scheme[3] + " !important;\n\tbackground-color: " + scheme[1] + " !important;\n}\n");
        outSB.append(".about-dialog div {\n\tcolor: " + scheme[3] + " !important;\n}\n");
	   
        outSB.append(".ui-dialog .ui-dialog-titlebar-close {\n\tbackground-color: " + scheme[2] + " !important;\n}\n");
        outSB.append(".ui-dialog .ui-dialog-titlebar-button {\n\tbackground-color: " + scheme[2] + " !important;\n}\n");
        
        outSB.append(".ui-dialog .ui-dialog-titlebar-close {\n\tbackground-color: " + scheme[2] + " !important;\n}\n");
        
        outSB.append(".w2ui-tabs .w2ui-tab {\n\tborder: 1px solid " + scheme[2] + " !important;\n\tborder-bottom: 1px solid" + scheme[2] + "!important;\n}\n");
//        outSB.append(".w2ui-tabs .w2ui-tab {\n\tbackground-color: " + scheme[0] + ";\n}}");
        outSB.append(".w2ui-tabs table {\n\tborder-bottom: 1px solid " + scheme[2] + " !important;\n}\n");
//        outSB.append(".w2ui-tabs .w2ui-tab.active {\n\tbackground-color: " + scheme[1] + " !important;\n}");
//        outSB.append(".w2ui-tabs .w2ui-tab.active {\n\tborder-bottom: 1px solid transparent !important;\n}");
        outSB.append(".w2ui-tab-add:hover {\n\tborder: 1px solid " + scheme[2] + " !important;\n}\n");
        outSB.append(".w2ui-scroll-wrapper {\n\tbackground-color: " + scheme[0] + ";\n}\n");
        
        outSB.append(".w2ui-tab-rename-input {\n\tborder-left: 1px solid " + scheme[4] + " !important;\n\tborder-top: 1px solid " + scheme[4] + " !important;\n\tborder-right: 1px solid " + scheme[2] + " !important;\n\tborder-bottom: 1px solid " + scheme[2] + " !important;\n}\n");
        outSB.append(".w2ui-tabs .w2ui-tab-close {\n\tcolor: " + scheme[3] + ";\n}\n");
        
        
        
        
        response.setContentType("text/css");
        PrintWriter out = response.getWriter();
        try {
//            LOGGER.error("outSB:\n" + outSB.toString());
            out.print(outSB.toString());
        } finally {
            out.close();
        }
    }
}
