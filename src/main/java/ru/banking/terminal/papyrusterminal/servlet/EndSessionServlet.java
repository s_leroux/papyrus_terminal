package ru.banking.terminal.papyrusterminal.servlet;

import java.io.IOException;
import java.util.HashMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ru.banking.terminal.papyrusterminal.App;
import ru.banking.terminal.papyrusterminal.entities.Session;

/**
 *
 * @author roux
 */
public class EndSessionServlet extends HttpServlet{
    private static final Logger LOGGER = LoggerFactory.getLogger(EndSessionServlet.class);
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        LOGGER.trace("Это doPost, request sessionId: " + request.getParameter("sessionId"));
        String sessionId = request.getParameter("sessionId");
        /* Если такая сессия есть, удалить из списка.
            Если нет - ничего не делать. */
        if(sessionId != null && App.INSTANCE.getSessions().keySet().contains(sessionId)) {
            App.INSTANCE.getSessions().remove(sessionId);
        }
        response.getWriter().println("<response>OK</response>");
    }
}
