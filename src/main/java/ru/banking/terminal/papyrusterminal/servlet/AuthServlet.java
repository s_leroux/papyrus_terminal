/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.banking.terminal.papyrusterminal.servlet;

//import com.sun.org.apache.xml.internal.security.exceptions.Base64DecodingException;
//import com.sun.org.apache.xml.internal.security.utils.Base64;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.banking.terminal.papyrusterminal.App;
import ru.banking.terminal.papyrusterminal.entities.Session;

/**
 *
 * @author roux
 */
public class AuthServlet extends HttpServlet {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthServlet.class);

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        LOGGER.trace("Это doPost, request: " + request.getContentLength());
//        LOGGER.trace("Это doPost, request credentials: " + request.getParameter("credentials"));
        String encoded = request.getParameter("credentials");
        try {
            byte[] decoded = Base64.getDecoder().decode(encoded);
            String[] loginPassword = new String(decoded).split(" ");
            
//            LOGGER.trace("login: " + loginPassword[0] + ", password: " + loginPassword[1]);
            /* TODO Проверить по базе - когда таковая будет */
            JSONObject root = new JSONObject();
            if("user".equals(loginPassword[0]) && "user".equals(loginPassword[1])) {
                /* Пользователь найден */
//                LOGGER.trace("Пользователь user найден");
                root.put("success", true);
                
                String sessionId = UUID.randomUUID().toString();
                App.INSTANCE.getSessions().put(sessionId, new Session(sessionId, "user", "Юзер Юзерович Юзеров"));
                root.put("sessionId", sessionId);
                
                root.put("login", loginPassword[0]);
                root.put("password", loginPassword[1]);
                root.put("userName", "Юзер Юзерович Юзеров");
            } else if("admin".equals(loginPassword[0]) && "admin".equals(loginPassword[1])) {
                /* Пользователь найден */
//                LOGGER.trace("Пользователь admin найден");
                root.put("success", true);
                
                String sessionId = UUID.randomUUID().toString();
                root.put("sessionId", sessionId);
                App.INSTANCE.getSessions().put(sessionId, new Session(sessionId, "admin", "Админ Админович Админов"));
                
                root.put("login", loginPassword[0]);
                root.put("password", loginPassword[1]);
                root.put("userName", "Админ Админович Админов");
            } else {
                /* Пользователь НЕ найден */
                LOGGER.trace("Пользователь " + loginPassword[0] + " НЕ найден");
                root.put("success", false);
                root.put("message", "Неверное имя пользователя или пароль.");
            }
            
//            LOGGER.trace("JSON: " + root.toString());
            /* TODO Странно - если поменять тип контента на application/json, машинка перестаёт работать */
            response.setContentType("text/html;charset=UTF-8");
            PrintWriter out = response.getWriter();
            try {
                out.print(root.toString());
            } finally {
                out.close();
            }
        } catch (Exception ex) {
            LOGGER.error("Ошибка при декодировании Base64, " + ex.getClass().getSimpleName() + ": " + ex.getMessage());
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
