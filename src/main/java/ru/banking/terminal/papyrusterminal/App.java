package ru.banking.terminal.papyrusterminal;

import java.util.HashMap;
import java.util.Hashtable;
import javax.servlet.http.HttpServlet;
import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import ru.banking.terminal.papyrusterminal.entities.Session;

/**
 *
 * @author roux
 */
public class App extends HttpServlet {
    private static final Logger LOGGER  = LoggerFactory.getLogger(App.class);
    
    public static final App INSTANCE = new App();
    
    private static final HashMap<String, String[]> COLOR_SCHEMES = new HashMap<String, String[]>() {{
        put("greyscale", new String[]{"#E6E6E6", "#E1E1E1", "#C8C8C8", "#919191", "#5A5A5A"});
        put("scheme_01", new String[]{"#D3B7AB", "#DAC294", "#EEDFC5", "#51505B", "#2A2024"});
        put("scheme_02", new String[]{"#C99585", "#DBC9AB", "#8C9D75", "#37492D", "#161C10"});
        put("scheme_03", new String[]{"#D5D5D5", "#BBB271", "#C88647", "#A83B1F", "#5F0405"});
        put("scheme_04", new String[]{"#BAAA51", "#8CADB2", "#487774", "#2F5434", "#132908"});
        put("scheme_05", new String[]{"#F2F0EE", "#E5E1DB", "#CBC2B6", "#9D8678", "#66473D"});
    }};
    
    /*  Создаём новый набор id сессий.*/
    private HashMap<String, Session> sessions = new HashMap<String, Session>();
    
    /**
     * Конструктор
     */
    public App() {
        LOGGER.trace("Это App");
    }

    /**
     * @return the sessions
     */
    public HashMap<String, Session> getSessions() {
        return sessions;
    }

    /**
     * @param sessions the sessions to set
     */
    public void setSessions(HashMap<String, Session> sessions) {
        this.sessions = sessions;
    }

    /**
     * @return the COLOR_SCHEMES
     */
    public static HashMap<String, String[]> getCOLOR_SCHEMES() {
        return COLOR_SCHEMES;
    }
}
