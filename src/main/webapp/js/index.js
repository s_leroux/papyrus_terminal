var session = {}, provider, termSession;
var splitterConfig; // Текущая конфигурация сплиттера. Отдельно для каждой закладки.

//console.log("Это скрипт");
/* Читаем имя цветовой схемы из кукисов. Если такая есть - грузим её,
 * если нет - оставляем серую шкалу. */
var colorSchema = $.cookie('terminal-color-schema');
//console.log("colorSchema: " + colorSchema);
if(colorSchema) {
    $("link[href^='css/schema/']").attr('href', 'css/schema/getColorScheme?scheme=' + colorSchema);
    /* Обновить cookie */
    $.cookie('terminal-color-schema', colorSchema, {expires: 365});
} else {
//    console.log("Оставляем серую шкалу");
}

$(document).ready(function() {
//    console.log("document ready");
    
    w2utils.locale('ru-ru');
    
    /* Создаём панельку для дерева */
    createTreePanel();

    /* Проверяем кукисы */
    /* TODO Надо придумать, как их обновлять при активных действиях */
    var termSession = $.cookie('terminal-session');
        
//    console.log("Cookie termSession: " + termSession);
    
    /* Подгружаем провайдер */
    provider = new Provider();
    
    /* Проверяем сессию из кукисов */
    if(termSession) {
        provider.checkSession(termSession, function(result) {
//            console.log("Проверена сессия: " + result);
            this.session = JSON.parse(result);
//            console.log("session: " + JSON.stringify(this.session));
            if (this.session.sessionState === 'valid') {
//                console.log("Session valid");
                onSessionValid(this.session);
            } else {
//                console.log("Session invalid");
                /* Удаляем cookie */
                $.removeCookie("terminal-session");
//                console.log("Грузим дерево гостя.");
                provider.getMenuTree(null, function(treeContent) {
                    renderTree(treeContent, "terminal_guest_treeview");
                });
                renderSplitter();
            }
        });
    } else {
        /* Загрузить дерево */
        provider.getMenuTree(null, function(treeContent) {
//            console.log("treeContent: " + JSON.stringify(treeContent));
            renderTree(treeContent, "terminal_guest_treeview");
            /* Раскрываем дерево */
//            $("#treePanel").animate({
//                height: "toggle",
//            })
            renderSplitter();
        });
    }   
    
    /* Создаём диалог авторизации */
    createAuthDialog();
    
    /* Создаём диалог выбора цветовой схемы */
    createColorSchemaDialog();
    
    /* Создаём диалог "О терминале..." */
    createAboutDialog();
});

function createTreePanel() {
//    console.log("Это createTreePanel");
    
    $(function ($) {
        $('#treePanels').enhsplitter({
            handle: 'lotsofdots',
            vertical: true,
            position: 250,
            leftMinSize: 0, 
            leftMaxSize: 450,
            fixed: false
        });
    });
}

function onSessionValid(data) {
//    console.log("Это onSessionValid, result: " + JSON.stringify(data));
    
    /* Обновить кукисы */
//    console.log("this.termSession: " + this.termSession);
    $.cookie("terminal-session", this.termSession, {expires: 1});
    
    /* Поменять "Вход" на "Выйти" */
    $("#log-in-item").html(constants.LOG_OUT);
    /* Заменить "Вход не выполнен" на имя пользователя */
    $("#band-title").html(data.userName);
    $("#name").val("");
    $("#password").val("");
    
    /* Схлопываем дерево */
    $("#treePanel").animate({
        height: "toggle",
    }, function() {
        /* Также перегрузить дерево */
        provider.getMenuTree(data.login, function(treeContent) {
//            console.log("treeContent: " + JSON.stringify(treeContent));
            var login;
            if(session.login) {
                login = session.login;
            } else {
                login = "guest";
            }
            renderTree(treeContent, "terminal_"+login+"_treeview");
            /* Раскрываем дерево */
            $("#treePanel").animate({
                height: "toggle",
            })
        });
    });
    
    /* Раз сессия валидна, берём cookie сплиттера для данного пользователя */
//    console.log("Берём cookie сплиттера для данного пользователя, session.login: " + session.login);
//    var cookiename = "terminal_" + session.login + "_splitter_config";
//    console.log("cookiename: " + cookiename);
    var splitterCookie = $.cookie("terminal_" + session.login + "_splitter_config");
//    console.log("splitterCookie: " + splitterCookie);
    
    var spliterCookieObj;
    
    if(!splitterCookie) {
//        console.log("Создаём cookie сплиттера");
        spliterCookieObj = {
            tabs: [
                {
                    id: 'tab1',
                    text: 'Рабочий стол 1',
                    closable: true,
                    rows: [
                        {
                            id: 'row_1',
                            weigth: 50
                        }
                    ]
                },
            ]
        };
    } else {
        spliterCookieObj = JSON.parse(splitterCookie);
    }
    /* Если в какой-либо закладке полученного spliterCookieObj нет rows, создаём ряд по умолчанию */
    for(var i = 0; i < spliterCookieObj.tabs.length; i++) {
//        console.log("rows " + i + ": " + JSON.stringify(spliterCookieObj.tabs[i].rows));
        if(!spliterCookieObj.tabs[i].rows) {
            spliterCookieObj.tabs[i].rows = [
                {
                    id: 'row_1',
                    weigth: 50
                },
                {
                    id: 'row_2',
                    weigth: 50
                },    
            ]
        }
    }
    /* Обновляем или записываем cookie */
//    console.log("spliterCookieObj: " + JSON.stringify(spliterCookieObj));
//    console.log("Вызов saveSplitterConfig 1, tabs: " + JSON.stringify(spliterCookieObj.tabs));
    saveSplitterConfig(spliterCookieObj.tabs);
    
    /* Рисуем сплиттер (пока - только закладки) */
    renderSplitter(spliterCookieObj);
}

/**
 * Отрисовываем спилиттер (пока - только закладки)
 * @param {type} config
 * @returns {undefined}
 */
function renderSplitter(config) {
//    console.log("Это renderSplitter, config: " + JSON.stringify(config));
//    console.log("Это renderSplitter, tabs: " + JSON.stringify(config));
//    console.log("Param name: " + JSON.stringify(w2ui["splitteredTabs"]));
    
    /* Для начала очистим поле */
    if(w2ui["splitteredTabs"]) w2ui["splitteredTabs"] = null;
    $('#splitterTabs').html('');
    
    /* Отрисовываем закладки */
    if(config) {
        $('#splitterTabs').w2tabs({
            name: 'splitteredTabs',
            active: 'tab1',
            createTab: true,
            closable: true,
            tabs: config.tabs,
            tabContentsPrefix: 'splitterCtns',
            newTabPrefix: 'Рабочий стол ',
            onRename: function(event) {
//                console.log("Это onRename, event: " + JSON.stringify(event.target));
//                console.log("Это onRename, tabs: " + JSON.stringify(w2ui['splitteredTabs'].tabs));
                /* Закладываем cookie */
                var tabs = $.map(w2ui['splitteredTabs'].tabs, function(item) {
                    return {
                        id: item.id,
                        text: item.text,
                        closable: item.closable
                    }
                });
                console.log("Вызов saveSplitterConfig 2, tabs: " + JSON.stringify(tabs));
                saveSplitterConfig(tabs);
            },
            onAdd: function(event) {
//                console.log("Это onAdd, event: " + JSON.stringify(event));
//                console.log("Это onAdd, tabs: " + JSON.stringify(w2ui['splitteredTabs'].tabs));
                /* Закладываем cookie */
                console.log("Вызов saveSplitterConfig 3");
                saveSplitterConfig();
            },
            onClose: function (event) {
//                console.log("Это onClose, event: " + JSON.stringify(event.object.id));
//                console.log("Это onClose, tabs:\n" + JSON.stringify(w2ui['splitteredTabs'].tabs));
                /* Надо из кукисов убрать закрытую закладку */
                var tabs = $.grep(w2ui['splitteredTabs'].tabs, function(value) {
                    return value.id != event.object.id;
                });
                tabs = $.map(tabs, function(item) {
                    return {
                        id: item.id,
                        text: item.text,
                        closable: item.closable
                    }
                });
                console.log("Вызов saveSplitterConfig 4, tabs: " + JSON.stringify(tabs));
                saveSplitterConfig(tabs);
            },
            onClick: function (event) {
    //            console.log("Target: " + event.target);
                $('#tabsBox').html(event.target);
            },
        });
    } else {
        $('#splitterTabs').w2tabs({
            name: 'splitteredTabs',
            active: 'tab1',
            createTab: true,
            closable: true,
            tabs: [
                {
                    id: 'tab1',
                    text: 'Рабочий стол 1',
                    closable: true,
                }
            ],
            newTabPrefix: 'Рабочий стол ',
            
            onClick: function (event) {
    //            console.log("Target: " + event.target);
                $('#tabsBox').html(event.target);
            },
            onClose: function (event) {
                console.log("Это onClose");
            }
        });
    }
    
    /* Собственно сплиттер - сначала прочитать в cookies информацию о панелях сплиттера. */
//    console.log("Отрисовываем собственно сплиттер, Tabs config: " + JSON.stringify(config));
//    if(config) {
//        console.log("Config есть, tabs: " + config.tabs.length);
//        for(var i = 0; i < config.tabs.length; i++) {
//            console.log("Tab " + config.tabs[i].id + " rows: " + JSON.stringify(config.tabs[i].rows));
//        }
//    }
//    $('#tabsBox').multiplesplitter({
//        vertical: false,
//        minHeight: 72,
//        minWidth: 144,
//        panels: [
//            {
//                weight: 50,
//                html: 'Lorem ipsum dolor sit amet, ei oratio nostrud adversarium eum, nec perpetua mnesarchum scripserit ne, prompta insolens corrumpit ei sit. Mei legimus suavitate ocurreret eu. Discere aliquam ne ius. Ferri atomorum omittantur vel ne. Est dico mnesarchum no. Ullum splendide ne est, audire nostrum comprehensam et eam, vis simul nominati argumentum ea.'
//            },
//            {
//                weight: 25,
//                html: 'Ius ex soleat accusam laboramus. Aliquid suavitate definitiones cu sea, qui te inani elitr salutatus. Et salutatus reprimique interesset vis. Graecis noluisse mei ex. Diam prima pertinacia sit cu, aperiri aliquid docendi ex est.'
//            },
//            {
//                weight: 50,
//                html: 'Ex platonem assueverit has, mel alii zril eu. At elit iuvaret partiendo eum, an vis nonumy salutatus, alia corpora reprimique id cum. Mel utinam timeam iracundia an, vix cu tale ornatus consulatu, vix in commodo ancillae. Fabulas corpora ius ex.'
//            },
//        ],
//        onSplitterResize: function(event) {
//            console.log("Это onSplitterResize");
//        }
//    });
}

/**
 * Добавляем ряд сверху
 * @returns {undefined}
 */
function addRow() {
//    alert("Это addRow");
    var tabs = $.map(w2ui['splitteredTabs'].tabs, function(item) {
        return {
            id: item.id,
            text: item.text,
            closable: item.closable
        }
    });
    console.log("tabs: " + JSON.stringify(tabs));
}

/**
 * Записываем в кукисы конфигурацию сплиттера.
 * Если задано - то, что задано, если нет - текущую.
 * @returns {undefined}
 */
function saveSplitterConfig(tabs) {
//    console.log("Это saveSplitterConfig, tabs: " + JSON.stringify(tabs));
    if(!tabs) {
        tabs = $.map(w2ui['splitteredTabs'].tabs, function(item) {
            return {
                id: item.id,
                text: item.text,
                closable: item.closable,
                rows: [
                    {
                        id: 'row_1',
                        weigth: 50
                    }
                ]
            }
        });
    }
//    console.log("Это saveSplitterConfig, splitterConfig: " + JSON.stringify(tabs));
    $.cookie("terminal_" + session.login + "_splitter_config", JSON.stringify({
        tabs: tabs
    }), {expire: 365});
}

/**
 * Сюда надо впихнуть вообще ВСЮ обработку OK на диалоге авторизации.
 * @param {type} result
 * @returns {undefined}
 */
function onAuthOK(result) {
//    console.log("Это onAuthOK, result: " + JSON.stringify(result));
//    console.log("name: " + $("#name").val() + ", type: " + $("#name").attr('type'));
//    console.log("PW: " + $("#password").val() + ", type: " + $("#password").attr('type'));
    /* Записать сессию */
    session.login = result.login;
//    session.password = result.password;
    session.id = result.sessionId;
    session.user = result.userName;
    
    /* Записать cookies */
    $.cookie("terminal-session", result.sessionId, {expires: 1});/* Само протухает через сутки */
    
    /* Поменять "Вход" на "Выйти" */
    $("#log-in-item").html(constants.LOG_OUT);
    /* Заменить "Вход не выполнен" на имя пользователя */
    $("#band-title").html(session.user);
    $("#name").val("");
    $("#password").val("");
    
    /* Схлопываем дерево */
    $("#treePanel").animate({
        height: "toggle",
    }, function() {
        /* Также перегрузить дерево */
        provider.getMenuTree(result.login, function(treeContent) {
//                            console.log("treeContent tree: " + JSON.stringify(treeContent.tree.length));
            var login;
            if(result.login) {
                login = result.login;
            } else {
                login = "guest";
            }

            renderTree(treeContent, "terminal_"+login+"_treeview");
            /* Раскрываем дерево */
            $("#treePanel").animate({
                height: "toggle",
            })
        });
    });
    
    var splitterCookie = $.cookie("terminal_" + result.login + "_splitter_config");
    var spliterCookieObj;
    /* Если в кусисах ничего нет, создать новый объект */
    if(!splitterCookie) {
        spliterCookieObj = {
            tabs: [
                {
                    id: 'tab1',
                    text: 'Рабочий стол 1',
                    closable: true,
                    rows: [
                        {
                            id: 'row_1',
                            weigth: 50,
                        },
                    ]
                },
            ]
        };
    } else {
        spliterCookieObj = JSON.parse(splitterCookie);
    }
//    console.log("Закладываем cookies, spliterCookieObj: " + JSON.stringify(spliterCookieObj));
//    console.log("Вызов saveSplitterConfig 5, spliterCookieObj.tabs: " + JSON.stringify(spliterCookieObj.tabs));
    saveSplitterConfig(spliterCookieObj.tabs);
    renderSplitter(spliterCookieObj);
}

/**
 * Рендерим дерево
 * @param {type} treeContent
 * @param {type} cookie_name
 * @returns {undefined}
 */
function renderTree(treeContent, cookie_name) {
//    console.log("Рендерим дерево, cookie_name: " + cookie_name);
    
    /* Обновляем cookie дерева */
    $.cookie(cookie_name, $.cookie(cookie_name), {expires: 365});
    
    /* Генерим html дерева */
    var ejs = new EJS({url: 'templates/tree_1.ejs'});
    
    var treeHTML = $(ejs.render(treeContent));
    
    $("#menu_tree").html(treeHTML);

    $("#menu_tree").treeview({
        animated: "fast",
        collapsed: false,
        unique: false,
        persist: "cookie",
        cookie_name: cookie_name,
//        toggle: function() {
//            window.console && console.log("%o was toggled", this);
//        }
    });
}

/**
 * Запираем дерево
 * @returns {undefined}
 */
function lockTree() {
   $(".treeview").addClass("tree-disabled");
}

/**
 * Отпираем дерево
 * @returns {undefined}
 */
function unlockTree() {
    $(".treeview").removeClass("tree-disabled");
}