function createColorSchemaDialog() {
//    console.log("Это createColorSchemaDialog, colorSchema: " + colorSchema);
    
    $("#color-schema-item").on("click", function() {
        $("#properties-button").jqDropdown("hide");
//        $("#color-schema").dialog("open");
        
//        console.log("Открытие colorSchema: " + colorSchema);
        
        provider.getColorSchemes(colorSchema, function(result) {
//            console.log("result: " + JSON.stringify(result));
            var colorSchemeDialog = new EJS({url: 'templates/colorSchemaDialog_4.ejs'}).render(result);
//            console.log("colorSchemeDialog: " + colorSchemeDialog);
            if (!w2ui.colorschemeform) {
                $().w2form({
                    name: 'colorschemeform',
                    style: 'border: 0px; background-color: transparent;',
                    formHTML: colorSchemeDialog,
                    actions: {
                        "okColorScheme": function () {
                            w2popup.close();
                            colorSchema = $('input[name="colorScheme"]:checked').val();
//                            console.log("Ставится colorSchema: " + JSON.stringify(colorSchema));
                            $("link[href^='css/schema/']").attr('href', 'css/schema/getColorScheme?scheme=' + colorSchema);
                            /* Обновить cookie */
                            $.cookie('terminal-color-schema', colorSchema, {expires: 365});
                        },
                        "cancelColorScheme": function () {
                            w2popup.close();
                        }
                    }
                });
            }
            
            $().w2popup('open', {
                title   : 'Выбор цветовой схемы',
                body    : '<div id="colorform" style="width: 100%; height: 100%;"></div>',
                style   : 'padding: 15px 0px 0px 0px',
                width   : 350,
                height  : 240,
                speed: 0,
                showMax : false,
                onToggle: function (event) {
    //                    console.log("Это onToggle");
                    $(w2ui.colorschemeform.box).hide();
                    event.onComplete = function () {
                        $(w2ui.colorschemeform.box).show();
                        w2ui.colorschemeform.resize();
                    }
                },
                onOpen: function (event) {
                    event.onComplete = function () {
                        
                        // specifying an onOpen handler instead is equivalent to specifying an onBeforeOpen handler, which would make this code execute too early and hence not deliver.
                        $('#w2ui-popup #colorform').w2render('colorschemeform');
                        /* Ставим правильно радиобаттон */  
                        $('input[value="' + colorSchema + '"]').attr('checked', true);

                        /* Вешаем слушатель на ENTER, чтобы по нажатию отправлять данные */
                        $('#colorform').keypress(function(e) {
                           if(e.keyCode === $.ui.keyCode.ENTER) {
                               $(this).find("button:eq(0)").trigger("click");
                            } 
                        });
                    }
                }
            });
        });
            
    });
}

function createAboutDialog() {
//    console.log("Это createAboutDialog");
    
    /* Выводим диалог "О терминале..." */
    $("#about-program-item").on("click", function() {
//        console.log("Открываем диалог \"О терминале...\"");
        $("#properties-button").jqDropdown("hide");
//        $("#about-dialog").dialog("open");
        
        var aboutDialog = new EJS({url: 'templates/aboutDialog_2.ejs'}).render();
        if (!w2ui.aboutform) {
            $().w2form({
                name: 'aboutform',
                style: 'border: 0px; background-color: transparent;',
                formHTML: aboutDialog,
                actions: {
                }
            });
        }
        $().w2popup('open', {
//            title   : 'Выход',
            body    : '<div id="aboutform" style="width: 100%; height: 100%;"></div>',
            style   : 'padding: 0px',
            width   : 430,
            height  : 280,
            speed: 0,
//            showMax : false,
            showClose: false,
            onToggle: function (event) {
//                    console.log("Это onToggle");
                $(w2ui.aboutform.box).hide();
                event.onComplete = function () {
                    $(w2ui.aboutform.box).show();
                    w2ui.aboutform.resize();
                }
            },
            onOpen: function (event) {
                event.onComplete = function () {
                    // specifying an onOpen handler instead is equivalent to specifying an onBeforeOpen handler, which would make this code execute too early and hence not deliver.
                    $('#w2ui-popup #aboutform').w2render('aboutform');

                    /* Переносим фокус на кнопку OK */
                    $("#okExit").focus();

//                        /* Вешаем слушатель на ENTER, чтобы по нажатию отправлять данные */
//                        $('#exitform').keypress(function(e) {
//                           if(e.keyCode === $.ui.keyCode.ENTER) {
//                               $(this).find("button:eq(0)").trigger("click");
//                            } 
//                        });
                }
            }
        });
    });
}

function createAuthDialog() {
//    console.log("Это createAuthDialog");
    
    /* Выводим диалог авторизации */
    $("#log-in-item").on("click", function() {
        $("#band-title").jqDropdown("hide");
        /* Проверить, есть ли сессия */
//        console.log("session: " + JSON.stringify(session.sessionState));
//        console.log("Empty: " + $.isEmptyObject(session));
        if($.isEmptyObject(session) || (session.sessionState && session.sessionState == 'invalid')) {
            
            var authDialog = new EJS({url: 'templates/authDialog_3.ejs'}).render();
            if (!w2ui.loginform) {
//                console.log("Создаём loginform");
                $().w2form({
                    name: 'loginform',
                    style: 'border: 0px; background-color: transparent;',
                    formHTML: authDialog,
                    fields: [
                        { field: 'un', type: 'text', required: true },
                        { field: 'pwd', type: 'password', required: true }
                    ],
//                    record: { 
//                        username    : 'John',
//                        password     : 'Doe'
//                    },
                    actions: {
                        "okAuth": function () {
                            var username = $('#un').val(), password = $('#pwd').val();
//                            console.log("username: " + username + ", password: " + password);
                            if(!username || !password) {
//                                console.log("Поля логина или пароля пустые");
                                $("#auth-result_1").html(constants.EMPTY_LOGIN_OR_PASSWORD);
                            } else {
                                var err = this.validate();
                                if(!err.length) {
                                    /* Проверить реквизиты. Если неправильно - выдать сообщение */
                                    provider.checkAuth(username, password, function(err, result) {
//                                        console.log("Это CB, err: " + JSON.stringify(err) + ", result: " + JSON.stringify(result));
                                        if(err) {
//                                            console.log("Ошибка при проверке авторизации, " + err);
                                            $("#auth-result_1").html(constants.AUTH_ERROR);
                                        } else {
                                            if(!result.success) {
                                                $("#auth-result_1").html(result.message);
                                            } else {
//                                                console.log("Авторизация успешна");
                                                onAuthOK(result);

                                                /* Закрыть диалог */
                                                w2popup.close();
                                            }
                                        }
                                    });
                                } else {
                                    $("#auth-result_1").html(constants.UNRECOGNIZED_ERROR);
                                }
                            }
                        },
                        "cancelAuth": function () {
                            w2popup.close();
                        }
                    }
                });
            }
            
//            console.log("Открываем authform");
            
            $().w2popup('open', {
                title   : 'Авторизация',
                body    : '<div id="authform" style="width: 100%; height: 100%;"></div>',
                style   : 'padding: 15px 0px 0px 0px',
                width   : 350,
                height  : 220,
                speed: 0,
                showMax : false,
                onToggle: function (event) {
//                    console.log("Это onToggle");
                    $(w2ui.loginform.box).hide();
                    event.onComplete = function () {
                        $(w2ui.loginform.box).show();
                        w2ui.loginform.resize();
                    }
                },
                onOpen: function (event) {
                    event.onComplete = function () {
                        // specifying an onOpen handler instead is equivalent to specifying an onBeforeOpen handler, which would make this code execute too early and hence not deliver.
                        $('#w2ui-popup #authform').w2render('loginform');
                        
                        /* Очищаем поля */
                        $("#un, #pwd").val("");
                        
                        /* Вешаем на поля логина и пароля слушатель,
                        * чтобы гасить сообщение об ошибке при любых событиях в них */
                        $("#un, #pwd").on("keydown", function() {
                            if($("#auth-result_1").html()) {
                                $("#auth-result_1").html("");
                            }
                        });
                        
                        /* Вешаем слушатель на ENTER, чтобы по нажатию отправлять данные */
                        $('#w2ui-popup #authform').keypress(function(e) {
                           if(e.keyCode === $.ui.keyCode.ENTER) {
                               /* Переносим фокус на кнопку ENTER */
                                $(this).find("button:eq(0)").focus();
                                $(this).find("button:eq(0)").trigger("click");
                            } 
                        });
                    }
                }
            });
        } else {
            /* Вывести диалог о выходе */
//            console.log("Выводим диалог о выходе");
            var exitDialog = new EJS({url: 'templates/exitDialog_1.ejs'}).render();
            if (!w2ui.logoutform) {
//                console.log("Создаём logoutform");
                $().w2form({
                    name: 'logoutform',
                    style: 'border: 0px; background-color: transparent;',
                    formHTML: exitDialog,
                    actions: {
                        "okExit": function () {
//                            console.log("Это Exit");
                            
                            w2popup.close();
                            /* Отправить на сервер сигнал о том, что сессия завершена. */
                            provider.endSession(session.id);

                            session = {};
                            $("#band-title").html(constants.NOT_LOGGED_IN);
                            $("#log-in-item").html(constants.LOG_IN);
                            /* И кукисы */
                            $.removeCookie("terminal-session");

                            /* Схлопываем дерево */
                            $("#treePanel").animate({
                                height: "toggle",
                            }, function() {
                                /* Также перегрузить дерево */
                                provider.getMenuTree(session, function(treeContent) {
            //                        console.log("treeContent: " + JSON.stringify(treeContent));
                                    var login;
                                    if(session.login) {
                                        login = session.login;
                                    } else {
                                        login = "guest";
                                    }
                                    renderTree(treeContent, "terminal_"+login+"_treeview");
                                    /* Раскрываем дерево */
                                    $("#treePanel").animate({
                                        height: "toggle",
                                    });
                                });
                            });
                            /* Также очищаем сплиттер */
                            renderSplitter();
                        },
                        "cancelExit": function () {
                            w2popup.close();
                        }
                    }
                });
            }
            $().w2popup('open', {
                title   : 'Выход',
                body    : '<div id="exitform" style="width: 100%; height: 100%;"></div>',
                style   : 'padding: 15px 0px 0px 0px',
                width   : 350,
                height  : 180,
                speed: 0,
                showMax : false,
                onToggle: function (event) {
//                    console.log("Это onToggle");
                    $(w2ui.logoutform.box).hide();
                    event.onComplete = function () {
                        $(w2ui.logoutform.box).show();
                        w2ui.logoutform.resize();
                    }
                },
                onOpen: function (event) {
                    event.onComplete = function () {
                        // specifying an onOpen handler instead is equivalent to specifying an onBeforeOpen handler, which would make this code execute too early and hence not deliver.
                        $('#w2ui-popup #exitform').w2render('logoutform');
                        
                        /* Переносим фокус на кнопку OK */
                        $("#okExit").focus();
                        
//                        /* Вешаем слушатель на ENTER, чтобы по нажатию отправлять данные */
//                        $('#exitform').keypress(function(e) {
//                           if(e.keyCode === $.ui.keyCode.ENTER) {
//                               $(this).find("button:eq(0)").trigger("click");
//                            } 
//                        });
                    }
                }
            });
        }
    });
}

function saveInfo() {
    alert("Информация сохранена.");
}

function openForm(name) {
    /* Если дерево заперто, уходим */
    if($(".treeview.tree-disabled").length) return;
    alert("Открывается форма " + name);
}