/**
 * (Фейковый) провайдер данных.
 * @returns {Provider}
 */
var Provider = function() {
//    console.log("Это Provider");
    
    /**
     * Проверяем логин-пароль.
     * Если найдено, в переменную session пишем их и UUID сессии, полученный с сервера.
     * @param {type} login
     * @param {type} password
     * @param {type} cb
     * @returns {undefined}
     */
   this.checkAuth = function(login, password, cb) {
//       console.log("Это checkAuth, login: " + login + ", password: " + password);
//       console.log("Это checkAuth, login: " + login.length + ", password: " + password.length);
       if(login.length < 1 || password.length < 1) {
           cb(null, {
               success: false,
               message: constants.EMPTY_LOGIN_OR_PASSWORD
           });
       } else if(login.indexOf(" ") !== -1 || password.indexOf(" ") !== -1) {
            cb(null, {
                success: false,
                message: constants.LOGIN_OR_PW_CONTAINING_SPACE
           });
       } else {
            /* Посылаем post-запрос на сервер */
            var encoded;
            try {
                encoded = btoa(login + " " + password);
                $.post("auth", {"credentials": encoded}, function(result) {
                    var resultJSON = JSON.parse(result);
//                    console.log("Вызываем CB");
                    cb(null, resultJSON);
                });
            } catch (err) {
                cb(null, {
                    success: false,
                    message: constants.STRING_CONTAINS_INVALID_CHARACTER
                });
            }
       }
   };
   
   /**
    * Отправляем команду серверу на удаление сессии из списка.
    * Коллбэк не предусматривается
    * @param {type} sessionId
    * @returns {undefined}
    */
    this.endSession = function(sessionId) {
//       console.log("Это endSession, sessionId: " + sessionId);
       $.post("endSession", {
           sessionId: sessionId
       }, function(resp) {
//           console.log("Это ответ завершения сессии, resp: " + resp);
       });
   }
   
   /**
    * Проверяем, не протухла ли сессия
    * В данном исполнении - всегда свежа.
    * @param {type} sessionId
    * @param {type} cb
    * @returns {undefined}
    */
   this.checkSession = function(sessionId, cb) {
//       console.log("Это checkSession, sessionId: " + sessionId);
       
       $.post("checkSession", {
           sessionId: sessionId
       }, function(result) {
//           console.log("Ответ от сервлета checkSession: " + result);
           cb(result);
       });
   };
   
   /**
    * Затычка
    * Загружаем с сервера список имеющихся цветовых схем - в виде JSON.
    * @returns {undefined}
    */
    this.getColorSchemes = function(selectedScheme, cb) {
//        console.log("Это getColorSchemes, selectedScheme: " + selectedScheme);
        
        $.get("getAllSchemes", {
            selectedScheme: selectedScheme
        }, function(result) {
            cb(JSON.parse(result));
        });
   }
   
   this.getMenuTree = function(login, cb) {
//       console.log("Это getMenuTree, login: " + JSON.stringify(login));
       /* для Юзера вернём юзерское дерево, для Админа - админское */
       if(login && login === "admin") {
           cb({'tree':[
                {
                'name': 'Администрирование (Справочники)',
		'children': [
                    {
                        'name': 'Пользователи',
                        'title': 'Открыть таблицу \'Пользователи\'',
                        'action': 'openTable(\'Пользователи\')'
                    },
                    {
                        'name': 'Клиенты',
                        'title': 'Открыть таблицу \'Клиенты\'',
                        'action': 'openTable(\'Клиенты\')'
                    },
                    {
                        'name': 'Дилеры',
                        'title': 'Открыть таблицу \'Дилеры\'',
                        'action': 'openTable(\'Дилеры\')'
                    },
                    {
                        'name': 'Все клиенты',
                        'title': 'Открыть таблицу \'Все клиенты\'',
                        'action': 'openTable(\'Все клиенты\')'
                    },
                    {
                        'name': 'Инструменты',
                        'title': 'Открыть таблицу \'Все клиенты\'',
                        'action': 'openTable(\'Инструменты\')'
                    },
                    {
                        'name': 'Монитор подключений',
                        'title': 'Открыть форму \'Монитор подключений\'',
                        'action': 'openForm(\'Монитор подключений\')'
                    },
                    {
                        'name': 'Дополнительно',
                        'children': [
                            {
                                'name': 'Первый доп',
                                'action': 'openForm(\'Первый доп\')'
                            },
                            {
                                'name': 'Второй доп',
                                'action': 'openForm(\'Второй доп\')'
                            }
                        ]
                    }
		]
            },
            {
		'name': 'Торговая система',
		'children': [
                    {
                        'name': 'Сообщения',
                        'title': 'Открыть таблицу \'Сообщения\'',
                        'action': 'openTable(\'Сообщения\')'
                    },
                    {
                        'name': 'Подписки',
                        'title': 'Открыть таблицу \'Подписки\'',
                        'action': 'openTable(\'Подписки\')'
                    },
                    {
                        'name': 'Фильтр сигналов',
                        'title': 'Открыть форму \'Фильтр сигналов\'',
                        'action': 'openForm(\'Фильтр сигналов\')'
                    },
                    {
                        'name': 'Сигналы',
                        'title': 'Открыть таблицу \'Сигналы\'',
                        'action': 'openTable(\'Сигналы\')'
                    },
                    {
                        'name': 'История сигналов',
                        'title': 'Открыть таблицу \'История сигналов\'',
                        'action': 'openTable(\'История сигналов\')'
                    },
                    {
                        'name': 'Котировки',
                        'title': 'Открыть таблицу \'Котировки\'',
                        'action': 'openTable(\'Котировки\')'
                    }
		]
            },
            {
		'name': 'Команды',
		'children': [
                    {
                        'name': 'Добавить ряд',
                        'title': 'Добавить пустой ряд',
                        'action': 'addRow()'
                    },
                    {
                        'name': 'Управление',
                        'title': 'Открыть форму \'Управление\'',
                        'action': 'openForm(\'Управление\')'
                    },
                    {
                        'name': 'Сохранить информацию',
                        'title': 'Сохранить информацию',
                        'action': 'saveInfo()'
                    }
		]
	}]}
        );
       } else if(login && login === "user") {
           cb(
            {'tree':[
                {
                    'name': 'Команды',
                    'children': [
                        {
                            'name': 'Добавить ряд',
                            'title': 'Добавить пустой ряд',
                            'action': 'addRow()'
                        },
                    ]
                },
                {
		'name': 'Торговая система',
		'children': [
                    {
                        'name': 'Подписки',
                        'title': 'Открыть таблицу \'Подписки\'',
                        'action': 'openTable(\'Подписки\')'
                    },
                    {
                        'name': 'Сигналы',
                        'title': 'Открыть таблицу \'Сигналы\'',
                        'action': 'openTable(\'Сигналы\')'
                    },
                    {
                        'name': 'Сообщения',
                        'action': 'openTable(\'Сообщения\')'
                    },
                    {
                        'name': 'Котировки',
                        'title': 'Открыть таблицу \'Котировки\'',
                        'action': 'openTable(\'Котировки\')'
                    }
		]
            }]}
        );
       } else {
         cb({
        'tree': [
            {
                'name': 'Команды',
                'children': [
                    {
                        'name': 'Добавить ряд',
                        'title': 'Добавить пустой ряд',
                        'action': 'addRow()'
                    },
                ]
            },
            {
            'name': 'Команды A',
            'children': [
                {
                    'name': 'Сохранить информацию I',
                    'title': 'Сохранить информацию',
                    'action': 'saveInfo()'
                }
            ]
	},
        {
            'name': 'Команды B',
            'children': [
                {
                    'name': 'Сохранить информацию II',
                    'title': 'Ещё раз сохранить информацию',
                    'action': 'saveInfo()'
                }
            ]
	}
    ]
        });
       }
   }
   
   this.getTableConfig = function(tableName, cb) {
       var ret;
       if(tableName === 'Пользователи') {
           ret = {
               eTableName: 'Users',
               columns: [
                    { field: 'login', caption: 'Логин', size: '10%', sortable: true },
                    { field: 'fio', caption: 'ФИО', size: '30%', sortable: true },
                    { field: 'status', caption: 'Статус', size: '10%', sortable: true },
                    { field: 'organization', caption: 'Организация', size: '20%', sortable: true },
                    { field: 'email', caption: 'E-mail', size: '20%', sortable: true },
                    { field: 'phone', caption: 'Телефон', size: '100px', sortable: true },
                ],
                records: [
                    { recid: 1, login: 'admin', fio: 'Админов Админ Админович', status: 'Торг', organization: "Trade Union", email: 'jdoe@gmail.com', phone: '212-85-06' },
                    { recid: 2, login: 'user', fio: 'Юзеров Юзер Юзерович', status: 'Торг', organization: "Trade Union", email: 'crebbs@gmail.com', phone: '312-85-06' }
                ],
                searches: [
                    { field: 'login', caption: 'Логин', type: 'text' },
                    { field: 'fio', caption: 'ФИО', type: 'text' },
                    { field: 'status', caption: 'Статус', type: 'text' },
                    { field: 'organization', caption: 'Организация', type: 'text'},
                    { field: 'email', caption: 'E-mail', type: 'text' },
                    { field: 'phone', caption: 'Телефон', type: 'text' }
                ],
           };
           cb(ret);
       } else if(tableName === 'Клиенты') {
           ret = {
               eTableName: 'Clients',
               columns: [
                    { field: 'login', caption: 'Логин', size: '10%', sortable: true },
                    { field: 'fio', caption: 'ФИО', size: '30%', sortable: true },
                    { field: 'status', caption: 'Статус', size: '10%', sortable: true },
                    { field: 'organization', caption: 'Организация', size: '20%', sortable: true },
                    { field: 'email', caption: 'E-mail', size: '20%', sortable: true },
                    { field: 'phone', caption: 'Телефон', size: '100px', sortable: true },
                ],
                records: [
                    { recid: 2, login: 'user', fio: 'Юзеров Юзер Юзерович', status: 'Торг', organization: "Trade Union", email: 'crebbs@gmail.com', phone: '312-85-06' }
                ],
                searches: [
                    { field: 'login', caption: 'Логин', type: 'text' },
                    { field: 'fio', caption: 'ФИО', type: 'text' },
                    { field: 'status', caption: 'Статус', type: 'text' },
                    { field: 'organization', caption: 'Организация', type: 'text'},
                    { field: 'email', caption: 'E-mail', type: 'text' },
                    { field: 'phone', caption: 'Телефон', type: 'text' }
                ],
           }
           cb(ret)
       } else if(tableName === 'Дилеры') {
           ret = {
               eTableName: 'Dealers',
               columns: [
                    { field: 'login', caption: 'Логин', size: '10%', sortable: true },
                    { field: 'fio', caption: 'ФИО', size: '30%', sortable: true },
                    { field: 'status', caption: 'Статус', size: '10%', sortable: true },
                    { field: 'organization', caption: 'Организация', size: '20%', sortable: true },
                    { field: 'email', caption: 'E-mail', size: '20%', sortable: true },
                    { field: 'phone', caption: 'Телефон', size: '100px', sortable: true },
                ],
                records: [
                    { recid: 1, login: 'admin', fio: 'Админов Админ Админович', status: 'Торг', organization: "Trade Union", email: 'jdoe@gmail.com', phone: '212-85-06' },
                ],
                searches: [
                    { field: 'login', caption: 'Логин', type: 'text' },
                    { field: 'fio', caption: 'ФИО', type: 'text' },
                    { field: 'status', caption: 'Статус', type: 'text' },
                    { field: 'organization', caption: 'Организация', type: 'text'},
                    { field: 'email', caption: 'E-mail', type: 'text' },
                    { field: 'phone', caption: 'Телефон', type: 'text' }
                ],
           }
           cb(ret);
       } else {
           cb({})
       }
   }
}