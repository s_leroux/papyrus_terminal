/* Скрипт для утилит */



function showAlert(output_msg, title_msg) {
    if (!title_msg) {
        title_msg = 'Alert';
    }

    if (!output_msg) {
        output_msg = 'No Message to Display.';
    }

    $("<div></div>").html(output_msg).dialog({
        title: title_msg,
        resizable: false,
        modal: true,
        buttons: {
            "Ok": function() {
                $( this ).dialog( "close" );
            }
        }
    });
}


