/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.fn.multiplesplitter = function (options) {
//        console.log("Это multiplesplitter, options: " + JSON.stringify(options));
        /* Итак, в зависимости от переданного конфига делим элемент this на несколько панелей,
         * разделённых собственно полосками сплиттера */
        var settings = $.extend({}, $.fn.multiplesplitter.defaults, options);
        var container = this;
//        console.log("settings: " + JSON.stringify(settings));
        
        if(settings.panels && settings.panels.length > 0) {
//            console.log("Да, вставляем, settings.panels: " + JSON.stringify(settings.panels));
            var sumWeight = 0;
            $.each(settings.panels, function(){
                sumWeight += this.weight;
            });
            
            var panels = [];
            var strips = [];
//            console.log("vertical: " + settings.vertical);
//            console.log("container id: "+ container.attr('id'));
            $.each(settings.panels, function(item){
                if(settings.vertical) {
                    panels[item] = $('<div />')
                        .attr('id', "ms_panel_" + container.attr('id') + "_" + item)
                        .addClass('multisplitter_panel')
                        .css({'float': 'left'})
                        .appendTo(container);
                    if(this.html) {
                        panels[item].html(this.html);
                    }
                    //* Если это не последняя панель, добавим полосу */
                    if(item < settings.panels.length -1) {
                        strips[item] = $('<div />')
                            .attr('id', "ms_splitter_" + container.attr('id') + "_" + item)
                            .addClass('multisplitter_strip_vertical')
                            .css({'float': 'left'})
                            .appendTo(container)
                            .on('mousedown', function(event) {msStart(event);});
                    }
                } else {
                    panels[item] = $('<div />')
                        .attr('id', "ms_panel_" + container.attr('id') + "_" + item)
                        .addClass('multisplitter_panel')
                        .css({'float': 'top'})
                        .appendTo(container);
                    if(this.html) {
                        panels[item].html(this.html);
                    }
                    /* Если это не последняя панель, добавим полосу */
                    if(item < settings.panels.length -1) {
                        strips[item] = $('<div />')
                            .attr('id', "ms_splitter_" + container.attr('id') + "_" + item)
                            .addClass('multisplitter_strip_horizontal')
                            .css({'float': 'top'})
                            .appendTo(container)
                            .on('mousedown', function(event) {msStart(event);});
                    }
                }  
            });
            refresh();
            
            
            
            var tmp = {}
            
            function msStart(event) {
                tmp.startX = event.screenX;
                tmp.startY = event.screenY;
//                console.log("Это mvStart, tmp.startX: " + tmp.startX + ", tmp.startY: " + tmp.startY);
                tmp.splitterNo = parseInt($(event.target).attr('id').match( /.+_(\d+)/)[1]);
                tmp.previousPanelId = "ms_panel_" + container.attr('id') + "_" + tmp.splitterNo;
                tmp.previousPanelStartWidth = $('#' + tmp.previousPanelId).width();
                tmp.previousPanelStartHeight = $('#' + tmp.previousPanelId).height();
                
                tmp.nextPanelId = "ms_panel_" + container.attr('id') + "_" + (tmp.splitterNo+1);
                tmp.nextPanelStartWidth = $('#' + tmp.nextPanelId).width();
                tmp.nextPanelStartHeight = $('#' + tmp.nextPanelId).height();
                
                $(document).on('mousemove', msMove);
                $(document).on('mouseup', msStop);
                if (event.stopPropagation) event.stopPropagation(); else event.cancelBubble = true;
                if (event.preventDefault) event.preventDefault(); else return false;
            }

            function msMove(event) {
//                console.log("Это msMove, x: " + event.screenX + ", y: " + event.screenY);
                if(settings.vertical) {
                    var shift = event.screenX - tmp.startX;
                    var prevWidth = tmp.previousPanelStartWidth + shift;
                    var nextWidth = tmp.nextPanelStartWidth - shift;
                    if(prevWidth > settings.minWidth && nextWidth > settings.minWidth) {
                        $("#" + tmp.previousPanelId).width(prevWidth);
                        $("#" + tmp.nextPanelId).width(nextWidth);
                    }
                } else {
                    var shift = event.screenY - tmp.startY;
                    var prevHeight = tmp.previousPanelStartHeight + shift;
                    var nextHeight = tmp.nextPanelStartHeight - shift;
                    if(prevHeight > settings.minHeight && nextHeight > settings.minHeight) {
                        $("#" + tmp.previousPanelId).height(prevHeight);
                        $("#" + tmp.nextPanelId).height(nextHeight);
                    }
                }
            }

            function msStop(event) {
//                console.log("Это msStop");
                $(document).off('mousemove', msMove);
                $(document).off('mouseup', msStop);
                if(settings.onSplitterResize) settings.onSplitterResize(event);
            }
        }
        
        $(window).on('resize', function() {
            refresh();
        });
        
        /*
        * Пересчитываем высоты или ширины панелей
        */
        function refresh() {
//            console.log("Это refresh, vertical: " + settings.vertical + ", sumWeight: " + sumWeight);
//            console.log("container width: " + container.width() + ", " + container.height());
            var sumHeight = 0, sumWidth = 0;
            $.each(settings.panels, function(item){
                if(settings.vertical) {
                    var paneWidth = this.weight*(container.width() - (settings.stripWidth+2)*(settings.panels.length-1))/sumWeight;
                    sumWidth += paneWidth;
                    panels[item].css({'width': paneWidth});
                    if(strips[item]) {
                        strips[item].css({});
                    }
                } else {
                    var paneHeight = this.weight*(container.height() - (settings.stripWidth)*(settings.panels.length-1))/sumWeight;
                    sumHeight += paneHeight;
                    panels[item].css({'height': paneHeight});
                    if(strips[item]) {
                        strips[item].css({});
                    }
                }
            });
            $(".multisplitter_strip_vertical").css({'width': settings.stripWidth});
            $(".multisplitter_strip_horizontal").css({'height': settings.stripWidth});
        }
    }
    
    $.fn.multiplesplitter.defaults = {
        vertical: false,
        minWidth: 72,
        minHeight: 36,
        stripWidth: 4,
        html: null,
        onSplitterResize: null,
//        invisible: false,
//        fixed: false,
//        height: null,
//        onDragStart: $.noop,
//        onDragEnd: $.noop,
//        onDrag: $.noop
    };
})(jQuery);


