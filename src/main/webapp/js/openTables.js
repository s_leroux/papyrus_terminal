this.gridNames = [];

function openTable(tableName) {
    console.log("Это openTable, name: " + tableName);
    
    /* Если дерево заперто, уходим */
    if($(".treeview.tree-disabled").length) return;
    
    lockTree();
    
    provider.getTableConfig(tableName, function(tableConfig) {
        var that = this;
//        console.log("columns: " + JSON.stringify(tableConfig.columns));
//        console.log("records: " + JSON.stringify(tableConfig.records));
//        console.log("eTableName: " + JSON.stringify(tableConfig.eTableName));
        
        var prefix = "grid_"+ tableConfig.eTableName + "_";
        var gridNamesNo = getSuitableGridNamesNo(prefix);
        
        var gridName = prefix + gridNamesNo;
        this.gridNames.push(gridName);
        
        if(!w2ui[gridName]) {
//            console.log("Создаём грид " + gridName);
            var config = {
                grid: {
                    name: gridName,
                    columns: tableConfig.columns,
                    records: tableConfig.records,
                    show: {
//                        header         : true,
                        toolbar     : true,
//                        footer        : true,
//                        lineNumbers    : true,
//                        selectColumn: true,
//                        expandColumn: true
                    },
                    multiSearch: true,
                    searches: tableConfig.searches,
                    onDblClick: function (event) {
                        event.onComplete = function () {
                            console.log("Это двойной старательский, Selection: " + JSON.stringify(this.get(this.getSelection()) ) );
                        }
                    },
                    onClick: function (event) {
                        event.onComplete = function() {
//                            console.log("Это onClick");
                        }
                    }
                },
            };

            $(function () {
                $().w2grid(config.grid);
            });
        }
        
//        console.log("Открываем таблицу");

        w2floatable.open({
            title   : tableName,
            width   : 800,
            height  : 240,
            showMax : true,
            showDock: true,
            gridName: gridName,
            body    : '<div id="table_' + tableConfig.eTableName + '" style="position: absolute; left: 0px; top: 5px; right: 0px; bottom: 0px;"></div>',
            onOpen  : function (event) {
                event.onComplete = function () {
//                    alert("Open Complete 1, gridName: "+ gridName + ", tableName: " + tableName);
                    $('#' + that.tableId + ' #table_' + tableConfig.eTableName).w2render(gridName);
//                    alert("Open Complete 2");
                    w2ui[gridName].refresh();
                    unlockTree();
                };
            },
            onToggle: function (event) {
                event.onComplete = function () {
//                    console.log("Это onToggle");
                }
            },
            onClose: function (event) {
                event.onComplete = function (data) {
//                    console.log("data: " + JSON.stringify(data));
                    that.gridNames = $.grep(that.gridNames, function(value) {
                        return value != data.target;
                    });
                }
            }
        }, function(id) {
//            console.log("Это CB, id: " + id);
            that.tableId = id;
        });
    });
    
    function getSuitableGridNamesNo(prefix) {
        /* Проходим по массиву от 0 до 50 и ищем первый свободный номер */
//        console.log("Это getSuitableGridNamesNo this.gridNames: " + this.gridNames.length);
        for (var i = 0; i < this.gridNames.length; i++) {
            if($.inArray(prefix + i, this.gridNames) === -1) {
                return i;
            }
        }
        
        return this.gridNames.length;
    }
}