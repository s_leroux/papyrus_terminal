<%-- 
    Document   : index
    Created on : Nov 18, 2016, 9:25:30 PM
    Author     : roux
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Papyrus terminal</title>
        <link rel="icon" type="image/png" href="img/papyrus.png">
        
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <link rel="stylesheet" type="text/css" href="css/signin.css">
        <!--Подгрузка цветовой схемы-->
        <link rel="stylesheet" type="text/css" href="css/schema/grayscale.css">
        <!--<link rel="stylesheet" type="text/css" href="css/schema/getColorScheme">-->
        
<!--        библиотеки css-->
        <!--<link rel="stylesheet" type="text/css" href="css/lib/jquery-ui.css">-->
        <link rel="stylesheet" type="text/css" href="css/lib/jquery.dropdown.css">
        <link rel="stylesheet" type="text/css" href="css/lib/jquery.enhsplitter.css">
        <link rel="stylesheet" type="text/css" href="css/lib/jquery.multisplitter.css">
        <link rel="stylesheet" type="text/css" href="css/lib/jquery.treeview.css">
        <!--<link rel="stylesheet" type="text/css" href="css/lib/jquery.dataTables.min.css">-->
        <link rel="stylesheet" type="text/css" href="css/lib/w2ui-1.5.rc1.css">
        <!--<link rel="stylesheet" type="text/css" href="css/lib/screen.css">-->
        
<!--        javascript-библиотеки-->
        <script src="js/lib/jquery-3.1.1.js"></script>
        <script src="js/lib/jquery-ui.js"></script>
        <script src="js/lib/jquery.dropdown.min.js"></script>
        <script src="js/lib/ejs_production.js"></script>
        <script src="js/lib/jquery.enhsplitter.js"></script>
        <script src="js/lib/jquery.multiplesplitter.js"></script>
        <script src="js/lib/jquery.cookie.js"></script>
        <script src="js/lib/jquery.treeview.js"></script>
        <!--<script src="js/lib/jquery.dataTables.min.js"></script>-->
        <script src="js/lib/w2ui-1.5.rc1.js"></script>
        
        <script src="js/index.js"></script>
        <script src="js/dataProvider.js"></script>
        <script src="js/constants.js"></script>
        <script src="js/invocable.js"></script>
        <script src="js/utils.js"></script>
        
        <script src="js/openTables.js"></script>
    </head>
    
    <body>
        <div class="upper-band">
            <span id="band-title" data-jq-dropdown="#jq-dropdown-1">Вход не выполнен</span>
        </div>
        <div class="control-pane">
<!--            <input id="tree-cb" type="checkbox" title="Дерево"><label for="tree-cb">Дерево</label>-->
            <button id="properties-button" data-jq-dropdown="#jq-dropdown-2"></button>
        </div>
        
        <div id="jq-dropdown-1" class="jq-dropdown jq-dropdown-tip jq-dropdown-anchor-right">
            <ul class="jq-dropdown-menu">
                <li><span id="log-in-item">Вход</span></li>
                <!--<li class="jq-dropdown-divider"></li>-->
                <!--<li><span id="change-pw-item">Сменить пароль</span></li>-->
            </ul>
        </div>
        
        <div id="jq-dropdown-2" class="jq-dropdown jq-dropdown-tip jq-dropdown-anchor-right">
            <ul class="jq-dropdown-menu">
                <li><span id="color-schema-item">Цветовая схема...</span></li>                
                <li><span id="about-program-item">О терминале...</span></li>                
            </ul>
        </div>
        
        <div id="treePanels">
            <div id="treePanel">
                <ul id="menu_tree" class="treeview"></ul>
            </div>
            <div id="splitterPanel">
                <div id="splitterTabs"></div>
                <!--<div id="selectedtabsBoxTab"></div>-->
                <!--<div id="tabsBox"></div>-->
            </div>
        </div>
    </body>
</html>