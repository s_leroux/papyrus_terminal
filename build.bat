set JAVA_HOME=C:\Program Files\Java\jdk1.8.0_171

net stop Tomcat8

call "%M2_HOME%\bin\mvn"  -f pom.xml clean package > build.log

rmdir /S/Q "C:\Program Files\Apache Software Foundation\Tomcat 8.5\webapps\PapyrusTerminal"
del "C:\Program Files\Apache Software Foundation\Tomcat 8.5\webapps\PapyrusTerminal.war"
del "C:\Program Files\Apache Software Foundation\Tomcat 8.5\logs\*.log"
del "C:\Program Files\Apache Software Foundation\Tomcat 8.5\logs\*.txt"

copy /y "target\PapyrusTerminal-1.0-SNAPSHOT.war" "C:\Program Files\Apache Software Foundation\Tomcat 8.5\webapps\PapyrusTerminal.war"

::pause

net start Tomcat8